FROM openjdk:8-jdk-alpine
MAINTAINER ibrahima.birane.faye <ifaye@gainde2000.sn>
COPY ./build/libs/*.jar /app/cars-api.jar
CMD ["java", "-jar", "/app/cars-api.jar"]
EXPOSE 8080